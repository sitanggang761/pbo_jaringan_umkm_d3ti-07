/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import main.NewHibernateUtil;
import org.hibernate.Session;
import model.Barang;
import org.hibernate.Query;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class CreateBarangController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
//    public static int id_toko;
    
    public int id_toko = AksesKeTokoController.id_toko;
    private FileChooser fileChooser;
    private File file;
    private Path copy,files;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
//    id_toko = RegisTokoController.id_toko;
    
//    divisi.setItems(divisiList);
//    agama.setItems(agamaList);
//    implement = new pegawaiDao();
    fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.png","*.jpg","*jpeg","*.bmp")
    );
    
    
    }
    private String gambar;

    @FXML
    private AnchorPane pane;

        
    @FXML
    private TextField tfNamaBarang;

    @FXML
    private TextField tfHarga;

    @FXML
    private TextArea taDeskripsi;
    @FXML
    private ImageView lihatGambar;
    


    @FXML
    void pilihGambar(MouseEvent event) {
        try{
            file =  fileChooser.showOpenDialog(null);
        
            if(file != null){
                try {
                    BufferedImage bufferedImage = ImageIO.read(file);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    lihatGambar.setFitWidth(180);
                    lihatGambar.setFitHeight(110);
                    lihatGambar.setPreserveRatio(true);
                    lihatGambar.setImage(image);
                    gambar = file.getName();
                    files = Paths.get(file.toURI());
                    
                    System.out.println(gambar);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }

    @FXML
    public void btnAdd(MouseEvent event) {
        if (tfNamaBarang.getText().equals("") || tfHarga.getText().equals("") || taDeskripsi.getText().equals("") || gambar.equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        } else {
            saveImage();
            executeHQLQuery();
        }
    }
    
     private void executeHQLQuery(){
         try{
             Barang barang = new Barang();
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();    
             Query q = session.createSQLQuery("INSERT INTO BARANG (NAMA_BARANG ,GAMBAR ,HARGA ,DESKRIPSI ,ID_TOKO ) values(:tfNamaBarang,:gambar,:tfHarga,:taDeskripsi,:id_toko)");
             q.setParameter("tfNamaBarang", tfNamaBarang.getText());
             q.setParameter("tfHarga", tfHarga.getText());
             q.setParameter("taDeskripsi", taDeskripsi.getText());
             q.setParameter("gambar", gambar);
             q.setParameter("id_toko", id_toko);
             q.executeUpdate();
            session.getTransaction().commit();
            session.close();
             pindah();
         }catch(Exception e){
             System.out.println(e.getMessage());
         }
     }
     
     private void saveImage(){
         if (gambar != null) {
            try {
                File dir = new File(System.getProperty("user.dir"));
                copy = Paths.get(dir+"/src/gambar_produk/"+gambar);
                CopyOption[] options = new CopyOption[]{
                        StandardCopyOption.REPLACE_EXISTING,
                        StandardCopyOption.COPY_ATTRIBUTES
                };
                Files.copy(files, copy,options);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            
        }
     }
     
     private void pindah() throws IOException {
        Alert alert = new Alert(Alert.AlertType.NONE, "Data berhasil di daftarkan", ButtonType.OK);
        alert.setTitle("Success");
        alert.showAndWait();

//        AnchorPane pane = FXMLLoader.load(getClass().getResource(".fxml"));
//        panes.getChildren().setAll(pane);
    }
    
}
