/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.DashboardController.gambar2;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import main.NewHibernateUtil;
import model.Toko;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class DaftarUMKMController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    public String nama_toko2,gambar2, alamat2, nomor_telepon2, keterangan2;
    
    private FileChooser fileChooser;
    private File file;
    private Path copy,files;
    private String gambar;
     @FXML
    private AnchorPane pane;

    @FXML
    private Button btnSaranKomentar;

    @FXML
    private TableView<Toko> tableToko;

    @FXML
    private TableColumn<Toko, String> kolNamaToko;

    @FXML
    private TableColumn<Toko, String> kolAlamat;

    @FXML
    private Label txtNamaToko;

    @FXML
    private Label txtAlamat;

    @FXML
    private Label txtNoTelepon;
    
    @FXML
    private ImageView lihatGambar;
    
    @FXML
    private Label txtDeskripsi;
    
    
    private static final String QUERY = "from Toko";

    private ObservableList<Toko> tableData = FXCollections.observableArrayList();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.png","*.jpg","*jpeg","*.bmp")
    );
        
        display();
    } 
    
    
    @FXML
    void btnDaftarkanToko(ActionEvent event) throws IOException {
         AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/DaftarUMKM.fxml"));
        
        pane.getChildren().setAll(panes);
    }

    private void display() {
        executeHQLQuery(QUERY);
    }

    private void executeHQLQuery(String QUERY) {
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(QUERY);
            List resultList = q.list();
            displayList(resultList);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void displayList(List resultList) {
        try{
            tableData = FXCollections.observableArrayList();
            for(Object o : resultList){   
                Toko toko=  (Toko)o;
                tableData.add(toko);
            }
            kolNamaToko.setCellValueFactory(new PropertyValueFactory<>("namaToko"));
            kolAlamat.setCellValueFactory(new PropertyValueFactory<>("alamatToko"));
            
            tableToko.getColumns().clear();
            tableToko.setItems(tableData);
            tableToko.getColumns().addAll(kolNamaToko, kolAlamat);
            tableToko.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>()
            {
                @Override
                public void handle(javafx.scene.input.MouseEvent event) {
                    onClick();
                }

                



            }
            );
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void onClick() {
        if(tableToko.getSelectionModel().getSelectedItem() != null){
            Toko tokoSelect =  tableToko.getSelectionModel().getSelectedItem();
            this.nama_toko2 = tokoSelect.getNamaToko();
            this.alamat2 = tokoSelect.getAlamatToko();
            this.gambar2 = tokoSelect.getGambarToko();
            this.nomor_telepon2 = tokoSelect.getNomorTelepon();
            this.keterangan2 = tokoSelect.getKeterangan();
            
            txtNamaToko.setText(nama_toko2);
            txtAlamat.setText(alamat2);
            txtNoTelepon.setText(nomor_telepon2);
            txtDeskripsi.setText(keterangan2);
            
            File kk = new File("src/gambar_toko/"+gambar2);
            Image img = new Image(kk.toURI().toString());
            lihatGambar.setImage(img);
        }
    }
    
//    @FXML
//    void btnDaftarkanUmkm(ActionEvent event) throws IOException {
//         AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/AksesKeToko.fxml"));
//        
//        pane.getChildren().setAll(panes);
//  
//    }
    
     @FXML
    public void btnKeToko(ActionEvent event) throws IOException {
        try{
            AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/AksesKeToko.fxml"));     
            pane.getChildren().setAll(panes);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        

    }
    
    
/*=============================Search Product=================================*/
    
    public String QUERY_SEARCH = "FROM Toko WHERE ALAMAT_TOKO like '";
    @FXML
    private TextField tfCariToko;
    
    @FXML
    public void btnCariToko(ActionEvent event) {
        executeHQLQuerySearchToko(QUERY_SEARCH + tfCariToko.getText()+ "'");
    }
    
    
/*============================================================================*/

    private void executeHQLQuerySearchToko(String sql) {
        System.out.println(sql);
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            List resultList = q.list();
            displayListSearch(resultList);
        }catch(Exception e){
            
            System.out.println(e.getMessage());
        }
    }

    private void displayListSearch(List resultList) {
        try{
            tableData = FXCollections.observableArrayList();
            for(Object o : resultList){   
                Toko toko=  (Toko)o;
                tableData.add(toko);
            }
            kolNamaToko.setCellValueFactory(new PropertyValueFactory<>("namaToko"));
            kolAlamat.setCellValueFactory(new PropertyValueFactory<>("alamatToko"));
            
            tableToko.getColumns().clear();
            tableToko.setItems(tableData);
            tableToko.getColumns().addAll(kolNamaToko, kolAlamat);
            tableToko.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>()
            {
                @Override
                public void handle(javafx.scene.input.MouseEvent event) {
                    onClick();
                }

            }
            );
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    
    @FXML
    private void btnKeDaftarUMKM(ActionEvent event) throws IOException {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/DaftarUMKM.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
     @FXML
    private void btnSaranDanKomentar(ActionEvent event) {
           try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/SaranDanKomentar.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
     @FXML
    private void btnDashboard(ActionEvent event) {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Dashboard.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
    
    ///////////////////////Action button//////////////////
    
}
    

 
