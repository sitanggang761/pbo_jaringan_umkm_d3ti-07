/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.LoginController;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import main.NewHibernateUtil;
import model.User;
import model.Barang;
import model.Komentar;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class SaranDanKomentarController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    public static int id_user = LoginController.id_user;
    public static String nama_user = LoginController.nama_user;
    public String nama_user2, isi_komentar2,tanggal2;
    
    
    
    @FXML
    public AnchorPane pane;
    
    @FXML
    private Button btnSaranKomentar;

    @FXML
    private TableView<Komentar> tabelKomentar;

    @FXML
    private TableColumn<Komentar, String> kolNamaUser;

    @FXML
    private TableColumn<Komentar, String> kolKomentar;

    @FXML
    private TableColumn<Komentar, String> kolTanggal;

    @FXML
    private Label txtNama;

    @FXML
    private Label txtTanggal;

    @FXML
    private Label txtKkomentar;
    
    @FXML
    private TextArea tfIsiKomentar;
    
    
    private ObservableList<Komentar> tableData = FXCollections.observableArrayList();
    
     private static final String QUERY = "from Komentar";
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        display();
        System.out.println(getTanggal());
        
    }    
    private String getTanggal() {  
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
        Date date = new Date();  
        return dateFormat.format(date);  
    }  
    
    public String date = getTanggal();
        
    public void display(){
        executeHQLQuery(QUERY);
    }
    
    private void executeHQLQuery(String sql) {
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            List resultList = q.list();
            displayList(resultList);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void displayList(List resultList) {
        try{
            tableData = FXCollections.observableArrayList();
            for(Object o : resultList){   
                Komentar komentar =  (Komentar)o;
                tableData.add(komentar);
            }
            kolNamaUser.setCellValueFactory(new PropertyValueFactory<>("namaUser"));
            kolKomentar.setCellValueFactory(new PropertyValueFactory<>("isiKomentar"));
            kolTanggal.setCellValueFactory(new PropertyValueFactory<>("tanggalKomentar"));
            
            tabelKomentar.getColumns().clear();
            tabelKomentar.setItems(tableData);
            tabelKomentar.getColumns().addAll(kolNamaUser, kolKomentar, kolTanggal);
            tabelKomentar.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event) {
                    onClick();
                }

                



            }
            );
            
        }catch(Exception e){
            
        }
    }
    private void onClick() {
        if(tabelKomentar.getSelectionModel().getSelectedItem() != null){
            Komentar komentar =  tabelKomentar.getSelectionModel().getSelectedItem();
            this.nama_user2 = komentar.getNamaUser();
            this.tanggal2 = komentar.getTanggalKomentar();
            this.isi_komentar2 = komentar.getIsiKomentar();

           txtNama.setText(nama_user2);
           txtTanggal.setText(tanggal2);
           txtKkomentar.setText(isi_komentar2);
        }

    }
    
    
    
    
    

    
     @FXML
    private void btnKeDaftarUMKM(ActionEvent event) throws IOException {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/DaftarUMKM.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
     @FXML
    private void btnSaranDanKomentar(ActionEvent event) {
           try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/SaranDanKomentar.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
     @FXML
    private void btnDashboard(ActionEvent event) {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Dashboard.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    @FXML
    public void btnKirimKomentar(ActionEvent event) {
         if(tfIsiKomentar.getText().equals("")){
             Alert alert = new Alert(Alert.AlertType.NONE, "Form komentar harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
         }else{
             executeHQLSendComment();
         }
    }

    private void executeHQLSendComment() {
        try{
            Komentar komentar = new Komentar();
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            System.out.println(getTanggal());
            System.out.println(tfIsiKomentar.getText());
            System.out.println(nama_user);
            System.out.println(id_user);
            Query q = session.createSQLQuery("INSERT INTO KOMENTAR (TANGGAL_KOMENTAR ,ISI_KOMENTAR ,NAMA_USER ,ID_USER) values(:tanggal,:komentar,:nama,:id_user)");
            q.setParameter("tanggal", date);
            q.setParameter("komentar", tfIsiKomentar.getText());
            q.setParameter("nama", nama_user);
            q.setParameter("id_user", id_user);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            pindah();
        }catch(Exception e){
           System.out.println(e.getMessage());
        }
    }

    private void pindah() throws Exception{
        try{
            Alert alert = new Alert(Alert.AlertType.NONE, "Komentar telah di kirim", ButtonType.OK);
        alert.setTitle("Success");
        alert.showAndWait();
        display();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }
    
    
    
}
