/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import main.NewHibernateUtil;
import org.hibernate.Query;
import model.User;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class RegisterController implements Initializable {

    @FXML
    private AnchorPane pane;

    @FXML
    private TextField tfNama;

    @FXML
    private TextField tfUsername;

    @FXML
    private PasswordField tfPassword;

    @FXML
    private TextField tfEmail;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

   
    @FXML
    private void daftarAction(ActionEvent event) throws Exception {
        try{
            if(tfNama.getText().equals("") || tfUsername.getText().equals("") || tfPassword.getText().equals("") || tfEmail.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
        }else{
            executeHQLQuery();
        }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        
    }

    @FXML
    private void resetAction(ActionEvent event) {
        tfNama.setText("");
        tfUsername.setText("");
        tfPassword.setText("");
        tfEmail.setText("");
        
    }

    private void executeHQLQuery() {
        try{
            org.hibernate.Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            System.out.println(tfNama.getText());
            System.out.println(tfUsername.getText());
            System.out.println(tfPassword.getText());
            System.out.println(tfEmail.getText());
            Query q = session.createSQLQuery("INSERT INTO USER (NAMA ,USERNAME ,PASSWORD ,EMAIL ) values(:nama,:username,:password,:email)");
            q.setParameter("nama",tfNama.getText());
            q.setParameter("username",tfUsername.getText());
            q.setParameter("password",tfPassword.getText());
            q.setParameter("email",tfEmail.getText());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            pindah();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void pindah() throws IOException {
        Alert alert = new Alert(Alert.AlertType.NONE, "Akun berhasil di daftarkan. Silahkan login", ButtonType.OK);
        alert.setTitle("Success");
        alert.showAndWait();
         
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Login.fxml"));
        pane.getChildren().setAll(panes);
    }

    @FXML
    public void backToLogin(MouseEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Login.fxml"));
        
        pane.getChildren().setAll(panes);
    }
    
    
    
    
}
