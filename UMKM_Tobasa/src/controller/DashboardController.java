package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static controller.KelolaTokoController.deskripsi2;
import static controller.KelolaTokoController.gambar2;
import static controller.KelolaTokoController.harga2;
import static controller.KelolaTokoController.id_toko;
import static controller.KelolaTokoController.namaBarang2;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import main.NewHibernateUtil;
import model.Barang;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class DashboardController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private FileChooser fileChooser;
    private File file;
    private Path copy,files;
    private String gambar;
    
    
    public static String namaBarang2, gambar2, harga2, deskripsi2;
    
    
        @FXML
    private AnchorPane pane;

    @FXML
    private TableView<Barang> tabelBarang;

    @FXML
    private TableColumn<Barang, String> kolNamaProduk;

    @FXML
    private TableColumn<Barang, String> kolHarga;

    @FXML
    private TableColumn<Barang, String> kolGambar;

    @FXML
    private TableColumn<Barang, String> kolDeskripsi;

    @FXML
    private Label txtNamaProduk;

    @FXML
    private Label txtHarga;

    @FXML
    private Label txtDeskripsi;

    @FXML
    private ImageView lihatGambar;
    
    public Barang barang = new Barang();

    private ObservableList<Barang> tableData = FXCollections.observableArrayList();
    
    private static final String QUERY = "from Barang order by ID_BARANG desc";
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.png","*.jpg","*jpeg","*.bmp")
    );
        
        display();
    }    

    private void display() {
        executeHQLQuery(QUERY);
    }

    private void executeHQLQuery(String sql) {
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            List resultList = q.list();
            displayList(resultList);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void displayList(List resultList) {
        try{
            tableData = FXCollections.observableArrayList();
            for(Object o : resultList){   
                Barang barang =  (Barang)o;
                tableData.add(barang);
            }
            kolNamaProduk.setCellValueFactory(new PropertyValueFactory<>("namaBarang"));
            kolHarga.setCellValueFactory(new PropertyValueFactory<>("harga"));
            kolGambar.setCellValueFactory(new PropertyValueFactory<>("gambar"));
            kolDeskripsi.setCellValueFactory(new PropertyValueFactory<>("deskripsi"));
           
            tabelBarang.getColumns().clear();
            tabelBarang.setItems(tableData);
            tabelBarang.getColumns().addAll(kolNamaProduk, kolHarga, kolGambar, kolDeskripsi);
            tabelBarang.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event) {
                    onClick();
                }



            }
            );
//            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void onClick() {
        if(tabelBarang.getSelectionModel().getSelectedItem() != null){
            Barang barangSelect = tabelBarang.getSelectionModel().getSelectedItem();
            //this.id2 = barangSelect.getIdBarang();
            this.namaBarang2 = barangSelect.getNamaBarang();
            this.gambar2 = barangSelect.getGambar();
//            this.harga2 = Double.parseDouble(barangSelect.getHarga());
            Double dbl = Double.valueOf(barangSelect.getHarga());
            double harga3 = dbl.doubleValue();
            this.deskripsi2 = barangSelect.getDeskripsi();
            
            txtNamaProduk.setText(namaBarang2);
            String harga4 = Double.toString(harga3);
            txtHarga.setText(harga4);
            txtDeskripsi.setText(deskripsi2);
            
            //namaBarangTampil.setText(namaBarang2);
            
            File kk = new File("src/gambar_produk/"+gambar2);
            Image img = new Image(kk.toURI().toString());
            lihatGambar.setImage(img);
            
            
        }
    }
    
    
    ///////////////////////Action button//////////////////
    @FXML
    private void btnKeDaftarUMKM(ActionEvent event) throws IOException {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/DaftarUMKM.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
     @FXML
    private void btnSaranDanKomentar(ActionEvent event) {
           try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/SaranDanKomentar.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
     @FXML
    private void btnDashboard(ActionEvent event) {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Dashboard.fxml"));
        pane.getChildren().setAll(panes);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
    @FXML
    public void btnLogout(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Login.fxml"));
        pane.getChildren().setAll(panes);
    }
    
    
    ///////////////////////Action button//////////////////
    
        
}