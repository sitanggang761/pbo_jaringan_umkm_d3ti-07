/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import main.NewHibernateUtil;
import model.Barang;
import model.Toko;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class RegistrasiUMKMController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.png","*.jpg","*jpeg","*.bmp")
    );
    }    
    
    private FileChooser fileChooser;
    private File file;
    private Path copy,files;
    private String gambar;
    
    private static String NAMA_TOKO = AksesKeTokoController.nama_toko;
    
    @FXML
    public AnchorPane pane;
    
    @FXML
    private TextField tfNamaUmkm;

    @FXML
    private TextField tfPassword;

    @FXML
    private TextField tfEmail;

    @FXML
    private ImageView lihatGambar;

    @FXML
    private Button pilihGambar;

    @FXML
    private TextArea taKeterangan;

    @FXML
    private TextField tfNomorTelepon;

    @FXML
    private TextField tfAlamat;

    @FXML
    private Button btnDaftarUmkm;

   
    
    @FXML
    public void pilihGambar(ActionEvent event) {
        try{
            file =  fileChooser.showOpenDialog(null);
        
            if(file != null){
                try {
                    String namaFoto;
                    //namaFoto = tfNamaBarang.getText();
                    BufferedImage bufferedImage = ImageIO.read(file);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    lihatGambar.setFitWidth(126);
                    lihatGambar.setFitHeight(105);
                    lihatGambar.setPreserveRatio(true);
                    lihatGambar.setImage(image);
                    gambar = file.getName();
                    files = Paths.get(file.toURI());
                    
                    System.out.println(gambar);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void saveImage(){
         if (gambar != null) {
            try {
                File dir = new File(System.getProperty("user.dir"));
                copy = Paths.get(dir+"/src/gambar_toko/"+gambar);
                CopyOption[] options = new CopyOption[]{
                        StandardCopyOption.REPLACE_EXISTING,
                        StandardCopyOption.COPY_ATTRIBUTES
                };
                Files.copy(files, copy,options);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            
        }
     }
    
     @FXML
    private void btnDaftarUmkm(ActionEvent event) {
         if (tfNamaUmkm.getText().equals("") || tfPassword.getText().equals("") ||tfEmail.getText().equals("") ||gambar.equals("") || taKeterangan.getText().equals("") || tfNomorTelepon.getText().equals("") || tfAlamat.getText().equals("") ) {
          Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
            } else {
                
                saveImage();
                executeHQLQueryInsert();
            }
    }

    private void executeHQLQueryInsert() {
        try{
             Toko toko = new Toko();
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction(); 
            Query q = session.createSQLQuery("INSERT INTO TOKO (NAMA_TOKO ,PASSWORD ,EMAIL ,GAMBAR_TOKO ,KETERANGAN ,NOMOR_TELEPON ,ALAMAT_TOKO ) "
                    + "values(:namaToko,:password,:email,:gambar,:keterangan,:nomortelepon,:alamat_toko)");
            q.setParameter("namaToko",tfNamaUmkm.getText());
            q.setParameter("password",tfPassword.getText());
            q.setParameter("email",tfEmail.getText());
            q.setParameter("gambar",gambar);
            q.setParameter("keterangan",taKeterangan.getText());
            q.setParameter("nomortelepon",tfNomorTelepon.getText());
            q.setParameter("alamat_toko",tfAlamat.getText());
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
             pindah();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }

    private void pindah() throws IOException {
        try{
            Alert alert = new Alert(Alert.AlertType.NONE, "Data berhasil di daftarkan", ButtonType.OK);
            alert.setTitle("Success");
            alert.showAndWait();

            AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/AksesKeToko.fxml"));
            pane.getChildren().setAll(panes);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

    }
    
}
