/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.AksesKeTokoController;
import controller.AksesKeTokoController;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import main.NewHibernateUtil;
import model.Barang;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class KelolaTokoController implements Initializable {

    public static int id_toko = AksesKeTokoController.id_toko;
    
    public static int id2 = -1;
    
    public static String namaBarang2, gambar2, harga2, deskripsi2;
    public static String namaToko =  AksesKeTokoController.nama_toko;
    
    private FileChooser fileChooser;
    private File file;
    private Path copy,files;
    private String gambar;
     @FXML
    private TextField tfNamaBarang;

    
     
     
    @FXML
    private TextField tfHarga;
    
    @FXML
    private Label namaBarangTampil;
    
    TextField numberField = new TextField();
//    numberField.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
//    
//    tfHarga.setTextFormatter(new TextFormater<>(new NumberStrinf));

    @FXML
    private TextArea taDeskripsi;
    @FXML
    private ImageView lihatGambar;
    
    private static String NAMA_TOKO = AksesKeTokoController.nama_toko; 
    
    @FXML
    private Label txtNamaToko;
    
    
    @FXML
    public AnchorPane pane;
    @FXML
    private TableColumn<Barang, Integer> kolId;
    
    @FXML
    private TableColumn<Barang, String> kolNamaProduk;
    
    @FXML
    private TableColumn<Barang, String> kolGambar;
    @FXML
    private TableColumn<Barang, String> kolHarga;
    @FXML
    private TableColumn<Barang, String> kolDeskripsi;
    @FXML
    private TableView<Barang> tabelBarang;
    
    
    public Barang barang = new Barang();

    private ObservableList<Barang> tableData = FXCollections.observableArrayList();
    
    private static final String QUERY = "from Barang where ID_TOKO = "+id_toko;
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Image Files", "*.png","*.jpg","*jpeg","*.bmp")
    );
        
        display();
        txtNamaToko.setText(NAMA_TOKO);
    }    
    
    
    
    @FXML
    void pilihGambar(MouseEvent event) {
        try{
            file =  fileChooser.showOpenDialog(null);
        
            if(file != null){
                try {
                    String namaFoto;
                    //namaFoto = tfNamaBarang.getText();
                    BufferedImage bufferedImage = ImageIO.read(file);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    lihatGambar.setFitWidth(180);
                    lihatGambar.setFitHeight(110);
                    lihatGambar.setPreserveRatio(true);
                    lihatGambar.setImage(image);
                    gambar = file.getName();
                    files = Paths.get(file.toURI());
                    
                    System.out.println(gambar);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }
    
    
/*===================Create Product===================*/
    
    @FXML
    public void prosesAdd(ActionEvent event) {
        
        System.out.println("test 123");
//        try{
            if (tfNamaBarang.getText().equals("") || tfHarga.getText().equals("") || taDeskripsi.getText().equals("") || gambar.equals("")) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Semua field harus di isi", ButtonType.OK);
            alert.setTitle("Gagal");
            alert.showAndWait();
            } else {
                
                saveImage();
                executeHQLQueryInsert();
            }
//        }catch(Exception e){
//            System.out.println(e.getMessage());
//            System.out.println("Ada yang gaggal");
//        }
        
        
    }

    
    private void saveImage(){
         if (gambar != null) {
            try {
                File dir = new File(System.getProperty("user.dir"));
                copy = Paths.get(dir+"/src/gambar_produk/"+gambar);
                CopyOption[] options = new CopyOption[]{
                        StandardCopyOption.REPLACE_EXISTING,
                        StandardCopyOption.COPY_ATTRIBUTES
                };
                Files.copy(files, copy,options);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            
        }
     }
    private void executeHQLQueryInsert() {
        try{
             Barang barang = new Barang();
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();    
             Query q = session.createSQLQuery("INSERT INTO BARANG (NAMA_BARANG ,GAMBAR ,HARGA ,DESKRIPSI ,ID_TOKO ) values(:tfNamaBarang,:gambar,:tfHarga,:taDeskripsi,:id_toko)");
             q.setParameter("tfNamaBarang", tfNamaBarang.getText());
             q.setParameter("tfHarga", tfHarga.getText());
             q.setParameter("taDeskripsi", taDeskripsi.getText());
             q.setParameter("gambar", gambar);
             q.setParameter("id_toko", id_toko);
             q.executeUpdate();
            session.getTransaction().commit();
            session.close();
             pindah();
         }catch(Exception e){
             System.out.println(e.getMessage());
         }
    }
    
    private void pindah() {
        Alert alert = new Alert(Alert.AlertType.NONE, "Data berhasil di daftarkan", ButtonType.OK);
        alert.setTitle("Success");
        alert.showAndWait();
        display();
    }

/*======================================*/
     
  
    
    
    
    
    
    private void display() {
        executeHQLQuery(QUERY);
    }

    private void executeHQLQuery(String sql) {
        System.out.println(sql);
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            List resultList = q.list();
            displayList(resultList);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void displayList(List resultList) {
        try{
            tableData = FXCollections.observableArrayList();
            for(Object o : resultList){   
                Barang barang =  (Barang)o;
                tableData.add(barang);
            }
            kolId.setCellValueFactory(new PropertyValueFactory<>("idBarang"));
            kolNamaProduk.setCellValueFactory(new PropertyValueFactory<>("namaBarang"));
            kolHarga.setCellValueFactory(new PropertyValueFactory<>("harga"));
            kolGambar.setCellValueFactory(new PropertyValueFactory<>("gambar"));
            kolDeskripsi.setCellValueFactory(new PropertyValueFactory<>("deskripsi"));
           
            tabelBarang.getColumns().clear();
            tabelBarang.setItems(tableData);
            tabelBarang.getColumns().addAll(kolId,kolNamaProduk, kolHarga, kolGambar, kolDeskripsi);
            tabelBarang.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event) {
                    onClick();
                }

            }
            );
//            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    public void onClick(){
        if(tabelBarang.getSelectionModel().getSelectedItem() != null){
            Barang barangSelect = tabelBarang.getSelectionModel().getSelectedItem();
            this.id2 = barangSelect.getIdBarang();
            this.namaBarang2 = barangSelect.getNamaBarang();
            this.gambar2 = barangSelect.getGambar();
            
            this.harga2 = String.valueOf(barang.getHarga());
            
            Double dbl = Double.valueOf(barangSelect.getHarga());
            double harga3 = dbl.doubleValue();
            
            this.deskripsi2 = barangSelect.getDeskripsi();
            
            tfNamaBarang.setText(namaBarang2);
            String harga4 = Double.toString(harga3);
            
            tfHarga.setText(harga4);
            taDeskripsi.setText(deskripsi2);
            namaBarangTampil.setText(namaBarang2);
            
            File kk = new File("src/gambar_produk/"+gambar2);
            Image img = new Image(kk.toURI().toString());
            lihatGambar.setImage(img);
            
            
        }
    }
    
    @FXML
    private void hapusBarang(ActionEvent event) {
        if(id2 == -1){
            Alert alert = new Alert(Alert.AlertType.NONE, "Silahkan pilih data yang akan dihapus", ButtonType.OK); 
            alert.setTitle("Data tidak dipilih"); 
            alert.showAndWait();             
        }else{
            executeHQLQueryDelete();
            Alert alert = new Alert(Alert.AlertType.NONE, "Barang Telah diHapus", ButtonType.OK);
            alert.setTitle("Berhasil");
             alert.showAndWait();
        }
        display();
    }
    
    private void executeHQLQueryDelete(){
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createSQLQuery("DELETE FROM Barang WHERE ID_BARANG ="+id2);
            //q.setParameter("id", id2);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        
            
    }
    
    
    @FXML
    public void btnPerbarui(ActionEvent event) {
         saveImage();
         executeHQLQueryUpdate();
    }

    
    private void executeHQLQueryUpdate(){
        try{
            String namaBarang1 = tfNamaBarang.getText();
         String harga1 = tfHarga.getText();
         String deskripsi1 = taDeskripsi.getText();
         String gambar1 = gambar;
         
         System.out.println(id2);
         System.out.println(namaBarang1);
         System.out.println(harga1);
         System.out.println(deskripsi1);
         System.out.println(gambar1);
         
        Barang barang = new Barang();
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q =  session.createSQLQuery("update Barang set NAMA_BARANG ='"+namaBarang1
                    + "' ,GAMBAR = '"+gambar1
                    + "',HARGA = "+harga1
                    + ",DESKRIPSI = '"+deskripsi1
                    + "'where id_barang ="+id2);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
            backToMainPage();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }

    private void backToMainPage() {
        Alert alert = new Alert(Alert.AlertType.NONE, "Data berhasil di ubah", ButtonType.OK);
        alert.setTitle("Success");
        alert.showAndWait();
        display();
    }
    
    @FXML
    public void btnLogout(ActionEvent event) throws IOException {
        try{
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/AksesKeToko.fxml"));
        pane.getChildren().setAll(panes);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

    }
    
    
}
