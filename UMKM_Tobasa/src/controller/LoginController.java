/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.NewHibernateUtil;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class LoginController implements Initializable {

    public static int id_user;
    public static String nama_user;

    @FXML
    private AnchorPane pane;
    @FXML
    private TextField tfUsername;
    @FXML
    private TextField tfPassword;
    @FXML
    private Button btnReset;
    @FXML
    private Button btnLogn;
    @FXML
    private Button btnRegister;

    public static String QUERY_LOGIN  = " from User where username like '";
    
    public String USERNAME;
    public String PASSWORD;
    public String NICKNAME;
//    public static int 
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void btnReset(ActionEvent event) {
        tfUsername.setText("");
        tfPassword.setText("");
    }

    @FXML
    private void btnLogn(ActionEvent event) {
        try{
            startLogin(event);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

    }

   

    private void startLogin(ActionEvent event) {
        executeHQLQuery(QUERY_LOGIN + tfUsername.getText() + "'", event);
    }

    private void executeHQLQuery(String sql, ActionEvent event) {
    try{
        
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            System.out.println("Berhasil");
            List resultList = q.list();
            for(Object o : resultList){
                User user = (User) o;
                nama_user = user.getNama();
                id_user= user.getIdUser();
                NICKNAME = user.getNama();
                USERNAME = user.getUsername();
                PASSWORD = user.getPassword();
            }
           session.getTransaction().commit();
            if(USERNAME == null || PASSWORD == null ){
                autentikasi(" "," ", event);
            }else{
                autentikasi(USERNAME,PASSWORD,event);
            }
            
            //String test = tfUsername.getText();
            
        }catch(Exception e){
            //e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    private void autentikasi(String txtUsername, String txtPassword, ActionEvent event) throws IOException {
        if(txtUsername.equals(tfUsername.getText()) && txtPassword.equals(tfPassword.getText())){
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/Dashboard.fxml"));
            Scene scene = new Scene(loader.load());
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            //System.out.println("UDAH BISA");
//          Stage stage = (Stage) ((Node) event.getSource().getScene().getWindow());
            stage.setScene(scene);
           
        }
        
            else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Username atau password salah", ButtonType.OK);
            alert.setTitle("Username atau password salah");
            alert.showAndWait();
        }
    }
    
     @FXML
    private void btnRegister(ActionEvent event) throws IOException {
        
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Register.fxml"));
        
        pane.getChildren().setAll(panes);

    }
     
}
