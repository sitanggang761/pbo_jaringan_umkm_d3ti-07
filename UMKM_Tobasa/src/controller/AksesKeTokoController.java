/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.NewHibernateUtil;
import model.Toko;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 * FXML Controller class
 *
 * @author Ronaldo Sitanggang
 */
public class AksesKeTokoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    @FXML
    private AnchorPane pane;

    @FXML
    private TextField tfNamaToko;

    @FXML
    private TextField tfPasswordToko;
    public static String QUERY_ACCESS  = " from Toko where nama_toko like '";
    public static int id_toko;
    public static String nama_toko;
    
    
    public String NAMATOKO;
    public String PASSWORD;
   

    @FXML
    void prosesMasuk(ActionEvent event) {
         masukKeToko(event);
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    private void masukKeToko(ActionEvent event) {
        executeHQLQuery(QUERY_ACCESS + tfNamaToko.getText() + "'", event);
    }

    private void executeHQLQuery(String sql, ActionEvent event) {
        System.out.println(sql);
        try{
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(sql);
            System.out.println("Berhasil");
            List resultList = q.list();
            for(Object o : resultList){
                Toko toko = (Toko) o;
                NAMATOKO = toko.getNamaToko();
                PASSWORD = toko.getPassword();
                id_toko = toko.getIdToko();
                nama_toko = toko.getNamaToko();
                
            }
           session.getTransaction().commit();
                if(NAMATOKO == null || PASSWORD == null ){
                    autentikasi(" "," ", event);
                }else{
                    autentikasi(NAMATOKO,PASSWORD,event);

                }
            }catch(Exception e){
            //e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    private void autentikasi(String namatoko, String password, ActionEvent event) throws IOException {
        if(namatoko.equals(tfNamaToko.getText()) && password.equals(tfPasswordToko.getText())){
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/KelolaToko.fxml"));
            Scene scene = new Scene(loader.load());
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            System.out.println(id_toko);
            stage.setScene(scene);
           
        }
        
            else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Nama toko atau password salah", ButtonType.OK);
            alert.setTitle("Nama toko atau password salah");
            alert.showAndWait();
        }
    }
    
    
    @FXML
    public void klikRegisToko(MouseEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/RegistrasiUMKM.fxml"));
        pane.getChildren().setAll(panes);
    }
    
    @FXML
    public void btnKembali(ActionEvent event) throws IOException {
        AnchorPane panes = FXMLLoader.load(getClass().getResource("/UI/Dashboard.fxml"));
        pane.getChildren().setAll(panes);
    }
    
    
}
